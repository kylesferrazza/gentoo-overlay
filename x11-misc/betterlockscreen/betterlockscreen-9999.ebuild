# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="A simple lock script for i3lock"

HOMEPAGE="https://github.com/pavanjadhaw/betterlockscreen"
EGIT_REPO_URI="https://github.com/pavanjadhaw/betterlockscreen"

LICENSE="GPL-3 MIT"
SLOT="0"
KEYWORDS="amd64"

DEPEND="
	x11-misc/i3lock-color
	media-gfx/imagemagick
	x11-apps/xdpyinfo
	x11-apps/xrandr
	sys-devel/bc
	media-gfx/feh
"
RDEPEND="
	${DEPEND}
"

src_install() {
	dobin betterlockscreen
}
