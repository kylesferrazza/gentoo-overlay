# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="A simple shell script that surfaces clipboard history from clipster in Rofi."
HOMEPAGE="https://github.com/gilbertw1/roficlip"
EGIT_REPO_URI="https://github.com/gilbertw1/roficlip"

LICENSE="GPL-3 MIT"
SLOT="0"
KEYWORDS="amd64"

src_install() {
	dobin roficlip
}
