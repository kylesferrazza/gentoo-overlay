# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

inherit git-r3

DESCRIPTION="solarized version of Numix"
HOMEPAGE="https://github.com/Ferdi265/numix-solarized-gtk-theme"
EGIT_REPO_URI="https://github.com/Ferdi265/numix-solarized-gtk-theme"

LICENSE="GPL-3 MIT"
SLOT="0"
KEYWORDS="amd64"

RDEPEND="
	x11-themes/gtk-engines-murrine
"
DEPEND="
	${RDEPEND}
	dev-ruby/sass
	dev-ruby/ruby-glib2
	media-gfx/inkscape
	media-gfx/optipng
"

RESTRICT="binchecks strip"

src_install() {
	# use flags for each color
	#themes=( *.colors )
	themes=( SolarizedDarkBlue.colors )

	for theme in ${themes[@]/.colors/}; do
		make THEME="$theme" DESTDIR="$D" install
	done
}
